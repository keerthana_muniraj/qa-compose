/* The query lists all the handles that were received without respective fault types(STATE,WAIT,QUIT)*/

/* The revoke should always occur in pairs
 * The pairs are (STATE, REVOKE), (WAIT, REVOKE), (QUIT, REVOKE)
 * Hence the query
 */

SELECT
    s1.handle
FROM
    scribelog_datamart_system_messages_prepared_s3 s1
WHERE
    s1.fault_type = "REVOKE"
    and (
        s1.fault_type <> "STATE"
        or s1.fault_type <> "QUIT"
        or s1.fault_type <> "WAIT"
    ) MINUS
SELECT
    s2.handle
FROM
    scribelog_datamart_system_messages_prepared_s3 s2
WHERE
    (
        s2.fault_type = "STATE"
        or s2.fault_type = "WAIT"
        or s2.fault_type = "QUIT"
    )
    and s2.fault_type <> "REVOKE"

