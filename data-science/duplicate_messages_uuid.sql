/* The query lists all the rows with duplicate uuid */

/* The uuid is unique field, and the receipe is to find  all the duplicate rows.
 */

SELECT
    *
FROM
    scribelog_datamart_system_messages_prepared_s3
WHERE
    uuid in (
        SELECT
            uuid
        FROM
            scribelog_datamart_system_messages_prepared_s3
        GROUP BY
            uuid
        HAVING
            COUNT(uuid) > 1
    )
    ORDER BY uuid