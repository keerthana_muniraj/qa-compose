/* Query which lists out the duplicate messages found across different
date folders(Partition_date) */

/* Note : The partition_date is "explicitly" named after the timestamp */

SELECT
    partition_date,
    cast(timestamp as String) as timestamp_string,
    uuid,
    serial_number,
    handle,
    fault_type
FROM
    scribelog_datamart_system_messages_prepared_s3
WHERE
    substr(partition_date, 1, 10) <> substr(cast(timestamp as String), 1, 10)
ORDER BY
    uuid
