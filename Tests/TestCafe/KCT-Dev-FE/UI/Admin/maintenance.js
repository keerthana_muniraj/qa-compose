/**
 * Created by akankshajain on 7/11/18.
 */

import {autoTester, uberTester} from './createuser';
import {Selector} from "testcafe";

fixture `Maintenance Tab`
    .page `https://www-dev.kuka-atx.com/app/#`
    .beforeEach(async t => {
    await t.useRole(autoTester);
});

test('Dashboard-Maintenance tab should be clicked and contain all filters', async t =>
{
    const maintenance_tab = await Selector('.tab-btn').child('.tab-title').withText('Maintenance');
    const checkbox = await Selector('.filter-item');

    await t
    // 1) Click on Maintenance tab
    .click(maintenance_tab)

    // 2) Maintenance checkbox count should be 3( Predictive , Corrective and Routine)
    // .expect(checkbox.count).eql(3)

    // 3) Checking if by default all checkboxes for Filters are 'checked'
    .expect(checkbox.withText('Routine maintenance').find('input[type=checkbox]').checked).ok()
    .expect(checkbox.withText('Corrective maintenance').find('input[type=checkbox]').checked).ok()
    .expect(checkbox.withText('Predictive maintenance').find('input[type=checkbox]').checked).ok()


    // 4) Uncheck all checkboxes
    .click(checkbox.withText('Routine maintenance').find('input[type=checkbox]'))
    .click(checkbox.withText('Corrective maintenance').find('input[type=checkbox]'))
    .click(checkbox.withText('Predictive maintenance').find('input[type=checkbox]'))

    // 5) Checking if all checkboses are unchecked now.
    .expect(checkbox.withText('Routine maintenance').find('input[type=checkbox]').checked).notOk()
    .expect(checkbox.withText('Corrective maintenance').find('input[type=checkbox]').checked).notOk()
    .expect(checkbox.withText('Predictive maintenance').find('input[type=checkbox]').checked).notOk()

});


// To execute this test file on terminal ,navigate to Admin directory and type $ node testrunner.js
