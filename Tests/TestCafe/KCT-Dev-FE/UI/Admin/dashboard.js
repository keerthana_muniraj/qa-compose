/*
 * Author : Keerthana
 */

import { autoTester, uberTester } from './createuser';
import {Selector} from "testcafe";

fixture `Kuka.Connect Login`
    .page `https://www-dev.kuka-atx.com/v3`;


test('The user should be able to see the Kuka Connect dashboard when logged in successfully', async t =>
{
   // const menu_button = Selector('span[class="menu-title"]');
    const title = await Selector('title').find('KUKA.Connect');

    await
    t
        .setTestSpeed(0.01)
        .useRole(autoTester)
        .expect(title).ok();
});