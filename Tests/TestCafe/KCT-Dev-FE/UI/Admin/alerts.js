/*
 * Author : Swapnil
 */

import {autoTester, uberTester} from './createuser';
import {Selector} from "testcafe";

fixture `Alerts tab`
    .page `https://www-dev.kuka-atx.com/v3/app/#`
    .beforeEach(async t => {
        await t.useRole(autoTester);
    });

test('Alerts tab should contain filter option', async t => {
    const alertTab = await Selector('.tab-title').withText('Alerts');
    const alertTypeFilter = await Selector('.label').withText('All selected');
    const filterValues1= await Selector('.check-title').withText('Quit');
    const filterValues2= await Selector('.check-title').withText('State');
    const filterValues3= await Selector('.check-title').withText('Wait');
    const filterValues4= await Selector('.check-title').withText('Predictive');
    const filterValues5= await Selector('.check-title').withText('Error');
    const filterValues6= await Selector('.check-title').withText('Warning');
    const filterValues7= await Selector('.check-title').withText('Info');
    const filterValues8= await Selector('.check-title').withText('Trace');

    console.log(" The Values are present in the filter");

    await t
    //1) Alerts tab test
        .expect(alertTab).ok()
        .click(alertTab)

        //2) Alerts Filter test
        .expect(alertTypeFilter).ok()
        .click(alertTypeFilter)
        .expect(filterValues1).ok()
        .expect(filterValues2).ok()
        .expect(filterValues3).ok()
        .expect(filterValues4).ok()
        .expect(filterValues5).ok()
        .expect(filterValues6).ok()
        .expect(filterValues7).ok()
        .expect(filterValues8).ok()
        .click(alertTypeFilter);
});
// To execute this test file on terminal ,navigate to Admin directory and type $ testcafe chrome navbar.js