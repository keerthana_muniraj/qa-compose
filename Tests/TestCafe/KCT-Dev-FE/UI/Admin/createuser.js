/*
 * Author : Keerthana
 */

import {Role} from 'testcafe';
import {LoginPage} from './auth';
//import {Selector} from 'testcafe';

const page = new LoginPage();
const loginPageUrl = 'https://connectdev-kukaprod.cs85.force.com/login/';

//create a role - autoTester to reuse in other testcases
export const autoTester = Role(loginPageUrl, async t =>
{
   //const login4 = await Selector('div[id="login-4"]');

    await t
        .typeText(page.login, 'auto.tester@kuka-atx.com.kukaconnectdev')
        .typeText(page.password, 'TestKCT800!')
        .click(page.signIn)
    //expect to see kuka login page
        .navigateTo('https://www-dev.kuka-atx.com/app/#/')

    /*Commenting out the below code as the user seems to be logged in at the above step.
     *However the login sceanrio for the new UI is very unstable. Please do not delete the below code
     * */

    //.click(Selector('div[id="app"]'))
    //.click(login4)
    // Not navigating and signing in
    //.navigateTo('https://www-dev.kuka-atx.com/app/#/');

},
{
    preserveUrl:true
})

// To execute this test file on terminal ,navigate to Admin directory and type $ testcafe chrome createuser.js
