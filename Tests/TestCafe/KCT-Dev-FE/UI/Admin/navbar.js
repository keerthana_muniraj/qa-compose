/*
 * Author : Keerthana
 *
 * Pre-requisite : Please run this command on terminal : npm install --save xpath-to-css
 */

import {autoTester, uberTester} from './createuser';
import {Selector} from "testcafe";
import xPathToCss from 'xpath-to-css';

fixture `Navigation bar`
    .page `https://www-dev.kuka-atx.com/app/#`
    .beforeEach (async t => {
    await t.useRole(autoTester);
});

test('Navigation bar should contain Menu, Notifications and Critical alerts', async t => {
    const nav_button = await Selector('#internal-notification-popover-root');
    const alerts_button = await Selector('#alerts-popover-root');
    setTimeout(nav_button, 10000);
    const menuIcon = await Selector('.menu-title');
    const menuList = await Selector('.menu-list');
    console.log("Verify the menu option on the dashboard");
    const administrationOption = await Selector('.menu-item').withText('Administration');
    const FAQVisible = await Selector('.menu-item').withText('FAQ');

    //we are trying to search for xpath and then converting it to css since the regular search of FAQ option (above) does not click/hover over the element accurately
    const xPathFAQ = await '//div[@id=\'menu-Menu-dropdown\']/ul/li[3]/a';
    const FAQ = xPathToCss(xPathFAQ);

    //we are trying to search for xpath and then converting it to css since the regular search does not click/hover over the element accurately
    const xPathGettingStarted = await '//div[@id=\'menu-Menu-dropdown\']/ul/li[4]/a';
    const gettingStarted = xPathToCss(xPathGettingStarted);
    const gettingStartedOption = await Selector('.menu-item').withText('Getting started');
    const adminPage = await Selector('#users-list-panel-button').withText('All users');

    const xPathLogout = '//div[@id="menu-Menu-dropdown"]/ul/li[6]/span/span';
    const logout = xPathToCss(xPathLogout);
    const logOutVisible = await Selector('.menu-item').withText('Logout');

    await t
        //1) Menu option test

        //2) Internal notification test
        .click(nav_button)
        //3) Critical alerts test
        .click(alerts_button)

        //4) Menu option should be present and clickable
        .expect(menuIcon.visible).ok()

        //5) Click on the Menu option and validate the menu list
        .click(menuIcon)
        .expect(menuList.visible).ok()

        //6) Click on the administration option and validate if the page is navigated to the admin settings
        .expect(administrationOption.visible).ok()
        .click(administrationOption)
        .expect(adminPage.visible).ok()

        //7) Navigate back to the home page and validate FAQ option
        .navigateTo('https://www-dev.kuka-atx.com/v3/app/#/')
        .click(menuIcon)
        .expect(FAQVisible.visible).ok()
        .hover(FAQ)
        //.click(FAQ) //-- Need to check if we need to open the pdf link

        //8) Navigate back to the home page and validate getting started option
        // .navigateTo('https://www-dev.kuka-atx.com/v3/app/#/')
        // .click(menuIcon)
        .expect(gettingStartedOption.visible).ok()
        .hover(gettingStartedOption)
        //.click(gettingStartedOption) -- Need to check if we need to open the pdf link

        //9) Navigate back to the home page and validate logout option
        // .navigateTo('https://www-dev.kuka-atx.com/v3/app/#/')
        // .click(menuIcon)
        .expect(logOutVisible.visible).ok()
        .hover(logout)
        .navigateTo('https://www-dev.kuka-atx.com/v3/app/#/');
});

// To execute this test file on terminal ,navigate to Admin directory and type $ testcafe chrome navbar.js
