/**
 * Created by Carlos Valdez on 8/14/18
 */

import {autoTester, uberTester} from './createuser';
import {Selector} from "testcafe";
import {SearchPage} from './searchpage'
fixture `Robot tests`
    .page `http://www-dev.kuka-atx.com/app/#`
    .beforeEach(async t => {
    await t.useRole(autoTester);
});

const page = new SearchPage();


test('Dropdown menu must be click-able', async t => {

     const dropdown_button = await Selector('.kuka-dropdown').nth(1)
     await t
         .maximizeWindow()
         .click(dropdown_button)
         .click(page.checkbox.withText('ready2_fasten').find('input[type=checkbox]'))
         .expect(page.checkbox.withText('ready2_fasten').find('input[type=checkbox]').checked).notOk()

 });

test("The search bar must be case insensitive, expect the results to be the same", async t=> {
    await
    t
        .setTestSpeed(0.1)
        // dismiss error statements may need to be deleted once dev gets more stable
        // simply execute .click(dismiss_error) if it is unstable.
        .click(page.search_bar)
        .typeText(page.search_bar, 'aGiLus')
        // validate
        .expect(page.robot.withText('Agilus')).ok()
        .click(page.robot.withText('Agilus'))
});


test("Select robot groups 'Future Test Robot Group', and 'PdM TestGroup'", async t=> {
    // click on the three groups,
    const allRobot_dropdown = await Selector('.kuka-dropdown').nth(0);
    const select_deselect_button = await Selector('.select-all').nth(0)
    await t
        .click(allRobot_dropdown)
        .click(select_deselect_button)
        .click(page.checkbox.withText('Future Test Robot Group').find('input[type=checkbox]'))
        .click(page.checkbox.withText('PdM Test Group').find('input[type=checkbox'))
        .click(allRobot_dropdown)
        // Validate result
        .expect(page.robot.withText('Future Test Robot Group')).ok()
        .expect(page.robot.withText('PdM Test Group')).ok()
});

test("Select 'ready2_fasten' in the dropdown menu for all types", async t=> {
    const allTypes_dropdown = await Selector('.kuka-dropdown').nth(1);
    const select_deselect_button = await Selector('.select-all').nth(1);
    await t
        .click(allTypes_dropdown)
        .click(select_deselect_button)
        .click(page.checkbox.withText('ready2_fasten').find('input[type=checkbox]'))
        .click(allTypes_dropdown)
        .click(page.robot.withText('R2F_3'))
});

//// Robot types dropdown menu section
test("Deselect one Robot group and validate the results", async t=> {
    const allRobot_dropdown = await Selector('.kuka-dropdown').nth(0)
    const select_deselect_button = await Selector('.select-all').nth(0)
    await t
        .click(allRobot_dropdown)
        .click(select_deselect_button)
        .click(page.checkbox.withText('Future Test Robot Group').find('input[type=checkbox]'))
        //validate.
        .click(page.robot.withText('OPS 20'))

});

test("Clicking on select / deselect all button must check / uncheck the options", async t=> {
    const allRobot_dropdown = await Selector('.kuka-dropdown').nth(1)
    const select_deselect_button = await Selector('.select-all').nth(1)
    await t
        .click(allRobot_dropdown)
        .click(select_deselect_button)
        .expect(page.checkbox.withText('KRC-Awesome').find('input[type=checkbox]').checked).notOk()
});

test("Select robot type 'ready2_fasten', and 'KRC-Awesome' must update the results page", async t=> {
    const allTypes_dropdown = await Selector('.kuka-dropdown').nth(1)
    const select_deselect_button = await Selector('.select-all').nth(1)
    await t
        .click(allTypes_dropdown)
        .click(select_deselect_button)
        .click(page.checkbox.withText('ready2_fasten').find('input[type=checkbox]'))
        .click(page.checkbox.withText('KR3R540 C4SR').find('input[type=checkbox]'))
        .click(allTypes_dropdown)
        // must validate
        .click(page.robot.withText('OPS29'))
});

test("Use the search bar to search for robot 'LBR iiwa 14 R820 MF SC FLR'", async t=> {
    await t
        // in case there are error messages, use .click(dismiss_error) to get rid of them.
        .click(page.search_bar)
        .typeText(page.search_bar, 'LBR iiwa 14 R820 MF SC FLR')
        // validate
        .click(page.robot.withText('LBR iiwa 14 R820 MF SC FLR'))
});

test(" Save a search and validate it", async t=> {
      const allGroups_dropdown = await Selector('.kuka-dropdown').nth(1)
      const allTypes_dropdown = await Selector('.kuka-dropdown').nth(2)
      await t
          .click(allGroups_dropdown)
          .click(page.checkbox.withText('Common Test Group').find('input[type=checkbox]'))
          .click(allTypes_dropdown)
          .click(page.checkbox.withText('KR6R900 C4SR FLR').find('input[type=checkbox]'))
          .click(page.save_button1) // save the search
          .typeText(page.save_input_name, 'Test Filter 1')
          .click(page.save_button2)
          .click(page.load_filters) // see the filters
          .expect(page.first_filter.withText('Test Filter 1'))
  });

test('Load a previously existing filter', async t=> {
     const load_filters = await Selector('.orange')
     const filterSelected = await Selector('.table-entry.item.clickable')
     const load_button = await Selector('.kuka-btn-label')
     await t
         .click(load_filters.withText('Load filters'))
         .click(filterSelected.withText('1'))
         .click(filterSelected.withText('r2f_filter'))
         .click(load_button.withText('Load'))
         .expect(page.robot.withText('R2F_3'))
});

// Not implemented yet. Keeping this code for reference once feature is implemented.
 // test('Delete a previously existing filter', async t=> {
 //     const load_filters = await Selector('.orange')
 //     const filterSelected = await Selector('.table-entry.item.clickable')
 //     const save_button2 = await Selector('.kuka-btn').nth(2)
 //     const filter = await Selector('.table-entry.item.clickable')
 //     await t
 //         .click(load_filters)
 //         .click(filterSelected)
 //         .expect(filter.withText())
// });

