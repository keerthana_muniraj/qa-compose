/*
 * Author : Keerthana
 */

import { Selector } from 'testcafe';

//Create login page class


export  class LoginPage {
    constructor () {
        this.login    = Selector('input[type="email"]');
        this.password = Selector('input[type="password"]');
        this.signIn   = Selector('input[type="submit"]');
    }
}

