/*
 * Author: Carlos
 */

import { Selector } from 'testcafe';

// Create search page parameters
export class SearchPage {
    constructor () {
        this.robottype_drowdown_button = Selector('.kuka-dropdown').nth(0)
        // this.alltype_drowdown_button = Selector('.kuka-dropdown').nth(1)
        this.checkbox = Selector('.option')
        this.search_bar = Selector('#search')
        this.robot = Selector('.table-entry.item.clickable')
        this.robot_name = Selector('.heading.major')
        this.save_button1 = Selector('.kuka-btn').nth(1)
        this.save_button2 = Selector('.kuka-btn').nth(2)
        this.save_input_name = Selector('a-input__field')
        this.load_filters = Selector('.orange')
        this.first_filter = Selector('.table-entry.item.clickable')
        this.dismiss_error = Selector('.close').nth(0) // for dev/prod instability
        this.filter_selected = Selector('.table-entry.item.clickable')
        this.load_button = Selector('kuka-btn-label')
    };

}
