#Getting started with testcafe

###Specific instructions to run the automated UI tests:

1) Set up SSH keys on bitbucket
2) Clone the QA repository and install the dependencies

   ```
   
   ```
3) Installation of testcafe
   
   Local installation
   
   ``
   $ cd Tests/TestCafe
   $ npm install --save-dev testcafe 
  
   ```
   npm installs testcafe and creates a file package-lock.json under TestCafe directory
   
   Current installed version 'testcafe@0.20.4'
   
   If testcafe version is updated please update the version on package-lock.json 
   
4) To run from IntelliJ
    Open the terminal and type the following
    
    ```
    $ cd Tests/TestCafe/KCT-Dev-FE/UI/Admin
    $ node testrunner.js
    ``` 
   
5) End to end (e2e) testing with testcafe

   Automated testers :
   
   - Auto Tester ( auto.tester@kuka-atx.com, TestKCT600! ) - Role : Admin
   
   - Maintenance tester ( maintenance.tester@kuka-atx.com, TestKCT500!) - Role : Maintenance
   
   - Technician tester ( technician.tester@kuka-atx.com, TestKCT500!) - Role : Technician
   
   - Uber Tester ( uber.tester@kuka-atx.com, TestKCT600! ) - Role : Uber 
   
   All of the admin related tests are carried out by Auto Tester. All the test setup requiring uber access will be carried out by Uber Tester.
   
   The login authentication for Kuka Connect is carried by Salesforce. Every time a uer logs in from a different machine, he or she is required to
   verify the account of auto tester and uber tester.
   
   *To verify the account*:
     
   Open the chrome browser, log in with Auto Tester's Google account. Login as auto tester on wwww-dev.kuka-atx.com. 
   During the login process you will be asked to verify your account through auto.tester@kuka-atx.com (Same username and password).
   Once you are verified and logged in, the account is logged in for 24 hours on salesforce until the user explicitly logs out of salesforce.
   To log out of salesforce, click on the link : https://connectdev-kukaprod.cs85.force.com/secur/logout.jsp?retUrl=http://localhost:63342/microservices/red-river-frontend/build/app/#/login
   The salesforce page opens up, log out by clicking on the auto tester profile and select 'log out'. 
   

### Overall Directory Structure

At a high level, the structure looks roughly like this:

```
Tests/
  |- TestCafe/
  |  |- KCT-Dev-UI/
  |  |  |- API
  |  |  |- UI/
  |  |  |  |-Admin/
  |  |  |  |-Uber
  |  |-node modules/
  |  |-POSE-Dev-FE
  |  |  |- UI
  |  |- Screenshots
  |-README.md 
```    

###Please find the description of each of the entries here :

**admin** : All the functionalities to be performed by an "admin role user - auto tester"    
    
**uber** : All the functionalities to be performed by an "uber role user - uber tester"
 