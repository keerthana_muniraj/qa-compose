
/*
 * Author : Keerthana
 */

const createTestCafe = require('testcafe');
testcafe = null;

createTestCafe()
    .then(tc => {
    testcafe = tc;
const runner = testcafe.createRunner();
//const report_file =
return runner
    .src(['./navbar.js','./dashboard.js','./robotsearch.js','./maintenance.js'])
    .browsers(['chrome'])
    .run({
        selectorTimeout  : 50000,
        assertionTimeout : 10000,
        pageLoadTimeout  : 50000,
        speed            : 0.5
    });
})
.then(failedCount => {
    console.log('Tests failed: ' + failedCount);
//stop testcafe server
testcafe.close();
process.exit(1);
});
